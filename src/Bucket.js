var class_Bucket = class_SceneObject.extend({
    type: null,
    trash_type: null,

    ctor: function(type) {
        this._super(type);
        this.trash_type = TRASH_TYPE.getTypeByBucket(type);
    }
});