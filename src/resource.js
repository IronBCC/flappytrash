var bucket_yellow = "res/yellow_bucket.png";
var bucket_green = "res/green_bucket.png";
var bucket_blue = "res/blue_bucket.png";
var cloud_1 = "res/cloud_1.png";
var tree_1 = "res/tree_1.png";
var tree_2 = "res/tree_2.png";
var tree_3 = "res/tree_3.png";
var house = "res/house.png";

var g_resources = [
    {src:bucket_yellow},
    {src:bucket_green},
    {src:bucket_blue},
    {src:cloud_1},
    {src:tree_1},
    {src:tree_2},
    {src:tree_3},
    {src:house}
];