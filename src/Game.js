/**
 * Created by IronBCC on 08.06.2014.
 */
const Game = {
    _gamePoints:0,
    _gameMenu:null,
    _trashQueue:null,
    getTrashQueue:function() {
        return Game._trashQueue;
    },
    generateGameQueue:function(count) {
        Game._trashQueue = new Array();
        for(var i=0;i<TRASH_TYPE.count;i++) {
            Game._trashQueue.push(
                TRASH_TYPE.getTrashByType(getRandomInt(0, TRASH_TYPE.count - 1))
            );
        }
        return Game._trashQueue;
    },

    startGame:function(count) {
//        Game._gamePoints = 0;
        Game.generateGameQueue(count);
        heroLayer.start();
    },
    restartGame:function() {
        Game.startGame(getRandomInt(3, 10));
    },
    getNextTrash:function() {
        if(!Game.checkQueue()) {
            return Game._trashQueue.pop();
        }
    },
    checkQueue:function() {
        if(!Game._trashQueue || Game._trashQueue.length <= 0) {
            Game.onQueueFinished();
            return true;
        }
        return false;
    },
    onGameOverTouch:function() {
        cc.Director.getInstance().replaceScene(Game.scene(getRandomInt(3, 10)));
    },
    onQueueFinished:function(){
        Game.restartGame();
    },
    onPlayerWithBarrier:function(player, barrel) {
//        if(!barrel) {
            cc.Director.getInstance().replaceScene(GameOver.scene(false));
//            Game.restartGame();
//        }
    },
    onPlayerWithCloud:function(player, cloud) {
        if(cloud.marked) return;
        cloud.marked = true;
        Game.onPointChange(BAD_POINTS);
        var parent = player.getParent();
        parent.addChild(UIHelper.flyText(BAD_POINTS, cloud));
    },
    onTrashWithBucket:function(trash, bucket) {
        var parent = trash.getParent();
        if(parent) {
            //bucket has same type
            if(bucket != trash) {
                Game.onPointChange(GOOD_POINTS);
                parent.addChild(UIHelper.flyText("+" + GOOD_POINTS, bucket));
            } else {
                Game.onPointChange(BAD_POINTS);
                parent.addChild(UIHelper.flyText(BAD_POINTS, bucket));
            }
        }
    },
    onPointChange:function(value){
        Game._gamePoints+=value;
        Game._gameMenu.updateScore(Game._gamePoints);
    }

};
var heroLayer;
const bgColor = new cc.Color4B(230, 230, 230, 255);
Game.scene = function (count) {
    var scene = cc.Scene.create();

    var size = cc.Director.getInstance().getWinSize();
    var back = new cc.LayerColor.create(bgColor, size.width, size.height);
    scene.addChild(back);

    Background = new class_Background();
    Background.init();

    heroLayer = new HeroLayer();
    heroLayer.init();

    Game._gameMenu = ScorePanel.create();

    scene.addChild(Background, 0, "Background");
    scene.addChild(heroLayer);
    scene.addChild(Game._gameMenu);

    Game.startGame(count | 5);
    return scene;
};