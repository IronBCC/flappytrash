/**
 * Created by IronBCC on 08.06.2014.
 */
var Splash = cc.Layer.extend({
    onEnter:function() {
        this._super();
        var size = screenSize();
        var child = cc.Sprite.create("res/splash.png");
        child.setScale(0.4);
        child.setPosition(size.width / 2,  size.height / 2);
        this.addChild(child);
        this.setTouchEnabled(true);
    },
    onTouchesEnded:function (touches, event) {
        Game.onGameOverTouch();
    }
});

Splash.create = function() {
    var splash = new Splash();
    splash.init();
    return splash;
}

Splash.scene = function() {
    var scene = cc.Scene.create();
    scene.addChild(cc.LayerColor.create(bgColor));
    scene.addChild(Splash.create());
    return scene;
}