var class_SceneObject = cc.Class.extend({
   type: null,
   sprite:null,

   ctor: function(type){
       this.type = type;
       this.sprite = cc.Sprite.create(type);
   },

    setPosition:function(x, y){
        return this.sprite.setPosition(x, y);
    },

    getPositionX:function(){
        return this.sprite.getPositionX();
    },

    getHeight:function(){
        return this.sprite.getContentSize().height;
    },

    getPosition:function(){
        return this.sprite.getPosition();
    },

    getScale: function(){
        return this.sprite.getScale();
    },

    getContentSize:function(){
        return this.sprite.getContentSize();
    },

    setPositionX:function(x){
        return this.sprite.setPositionX(x);
    },

    setScale:function(scale){
        return this.sprite.setScale(scale);
    },

    remove: function(){
        var parent =  this.sprite.getParent();
        if(parent)
            parent.removeChild(this.sprite);
    },

    addOn: function(layer){
        layer.addChild(this.sprite);
    }
});