/**
 * Created by veretennikov on 07.06.2014.
 */
TRASH_TYPE = {
    YELLOW:{
        type:0,
        path:"res/trash_yellow.png"
    },
    GREEN:{
        type:1,
        path:"res/trash_green.png"
    },
    BLUE:{
        type:2,
        path:"res/trash_blue.png"
    },
    count:3,
    getTrashByType:function(value) {
        switch (value) {
            case 0: return TRASH_TYPE.YELLOW;
            case 1: return TRASH_TYPE.GREEN;
            case 2: return TRASH_TYPE.BLUE;
        }
    },

    getTypeByBucket: function(bucket){
        switch(bucket){
            case bucket_yellow: return TRASH_TYPE.YELLOW;
            case bucket_green: return TRASH_TYPE.GREEN;
            case bucket_blue: return TRASH_TYPE.BLUE;
        }
    }
};

const GOOD_POINTS = 100;
const OK_POINTS = 50;
const BAD_POINTS = -50;