var class_Background = cc.Layer.extend({
    eyeX:0,
    eyeY:30,
    bucket_types:[bucket_yellow, bucket_green, bucket_blue],
    barrier_types:[tree_1, tree_2, tree_3, house],

    barriers:[],
    buckets: [],
    clouds: [],

    init:function () {
        this._super();

        this.eyeX = 100;
        this.scheduleUpdate();

        return true;
    },

    updateBuckets: function () {
        var size = cc.Director.getInstance().getWinSize();
        var x = 0;
        var new_buckets = [];
        for(var i in this.buckets) {
            x = this.buckets[i].getPositionX()
            this.buckets[i].setPositionX(x-1);
            if(x <= -50){
                this.buckets[i].remove();
            } else {
                new_buckets.push(this.buckets[i]);
            }
        }
        this.buckets = new_buckets;

        if(x===0 || (size.width - x) > 50 ){
            var bucket_type = this.bucket_types[getRandomInt(0, 2)];
            var bucket = new class_Bucket(bucket_type);
            var x = getRandomInt(0, 300);
            bucket.setPosition((size.width + x), 25);
            bucket.setScale(0.2);
            bucket.addOn(this);
            this.buckets.push(bucket);
        }
    },

    updateClouds: function () {
        var size = cc.Director.getInstance().getWinSize();
        var x = 0;
        var new_clouds = [];
        for(var i in this.clouds) {
            x = this.clouds[i].getPositionX()
            this.clouds[i].setPositionX(x-1);
            if(x <= -55){
                this.clouds[i].remove();
            } else {
                new_clouds.push(this.clouds[i]);
            }
        }
        this.clouds = new_clouds;

        if(x===0 || (size.width - x) > 55 ){
            var cloud = new class_Cloud(cloud_1);
            var x = getRandomInt(0, 300);
            cloud.setPosition((size.width + x), size.height - getRandomInt(20, 170));
            cloud.setScale(0.35);
            cloud.addOn(this);

            this.clouds.push(cloud);
        }
    },

    updateBarriers: function () {
        var size = cc.Director.getInstance().getWinSize();
        var x = 0;
        var new_barriers = [];
        for(var i in this.barriers) {
            x = this.barriers[i].getPositionX()
            this.barriers[i].setPositionX(x-1);
            if(x <= -50){
                this.barriers[i].remove();
            } else {
                new_barriers.push(this.barriers[i]);
            }
        }
        this.barriers = new_barriers;

        if(x===0 || (size.width - x) > 50 ){
            var barrier_type = this.barrier_types[getRandomInt(0, (this.barrier_types.length-1))];
            var barrier = new class_Barrier(barrier_type);
            var x = getRandomInt(0, 300);

            switch(barrier_type) {
                case tree_1:
                    barrier.setPosition((size.width + x), 60);
                    break;
                case tree_2:
                    barrier.setPosition((size.width + x), 35);
                    break;
                case tree_3:
                    barrier.setPosition((size.width + x), 50);
                    break;
                case house:
                    barrier.setPosition((size.width + x), 80);
                    break;
                default:
                    barrier.setPosition((size.width + x), 40);
            }

            barrier.setScale(0.4);
            barrier.addOn(this);
            this.barriers.push(barrier);
        }
    },

    update:function (dt) {
        this.eyeX = this.eyeX + 1;
        this.updateBarriers();
        this.updateBuckets();
        this.updateClouds();
    }
});