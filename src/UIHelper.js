/**
 * Created by IronBCC on 08.06.2014.
 */
function getRandomInt(min, max){
    return Math.floor(Math.random() * (max - min + 1));

}

var screenSize = function() {
    return cc.Director.getInstance().getWinSize();
};

var removeSelf = function(sprite) {
    var parent = sprite.getParent();
    if(parent)
        parent.removeChild(sprite);
};

const UIHelper = {

    flyText:function(text, sprite) {
        var txt = cc.LabelTTF.create(text, "Arial", 32);
        txt.setPosition(sprite.getPosition());
        var size = screenSize();
        txt.runAction(cc.Sequence.create(
            cc.MoveTo.create(1.5, new cc.Point(size.width / 2, size.height / 2)),
            cc.ScaleBy.create(0.5, 1.5),
            cc.CallFunc.create(removeSelf, txt, txt))
        );
        return txt;
    }
}