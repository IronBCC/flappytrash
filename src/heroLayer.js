/**
 * Created by veretennikov on 07.06.2014.
 */
const flapY = 50;
const downSpeed = -90;
const startX = 50;
const lowLevel = 10;
const bucketLevel = 80;

const flapRot = -45;
const downRot = 45;

const moveDown = cc.MoveTo.create(0, new cc.Point(startX, 0));

var trashFactory = {
    createRandom:function(hero) {
        var number = parseInt(Math.random() * TRASH_TYPE.count);
        switch (number) {
            case 0: return this.createTrash(TRASH_TYPE.YELLOW, hero);
            case 1: return this.createTrash(TRASH_TYPE.GREEN, hero);
            case 2: return this.createTrash(TRASH_TYPE.BLUE, hero);
        }
    },
    createNextTrash:function(hero) {
        var nextTrash = Game.getNextTrash();
        if(!nextTrash) return;
        return this.createTrash(nextTrash, hero);
    },
    createYellow:function(hero) {
        return this.createTrash(TRASH_TYPE.YELLOW, hero);
    },
    createGreen:function(hero) {
        return this.createTrash(TRASH_TYPE.GREEN, hero);
    },
    createBlue:function(hero) {
        return this.createTrash(TRASH_TYPE.BLUE, hero);
    },
    /**
     *
     * @param type {TRASH_TYPE}
     * @param hero
     * @returns {Trash}
     */
    createTrash:function(type, hero) {
        var sprite = new Trash();
        sprite.setType(type);
        sprite.init(type.path);
        sprite.setScale(0.6);
        sprite.setPosition(hero.getPosition());
        return sprite;
    }
};

var collideRect  = function(sprite) {
    var a = sprite.getContentSize();
    var p = sprite.getPosition();
    var width = a.width * sprite.getScale();
    var height = a.height * sprite.getScale();
    return cc.rect(p.x - width / 2, p.y - height / 2, width, height);
}

var Trash = cc.Sprite.extend({
   init:function(){
       this._super.apply(this, arguments);
//       this.setAnchorPoint(this._rect.width / 2, this._rect.height / 2);
       this.scheduleUpdate();
   },
    /**
     *
     * @param type {TRASH_TYPE}
     */
   setType:function(type) {
       this.type = type;
   },
   onMoveFinished:function(){
       cc.log("move finished");
   },
    update: function () {
        if(this._position.y > bucketLevel) return;
        if(this._position.y <= lowLevel) {
            Game.onTrashWithBucket(this, this);
            removeSelf(this);
            return;
        }
        var buckets = Background.buckets;
        var thisRect = collideRect(this);
        for (var i = 0; i < buckets.length; i++) {
            var obj = buckets[i];
             if(cc.rectIntersectsRect(thisRect, collideRect(obj))) {
                Game.onTrashWithBucket(this, obj);
                removeSelf(this);
            }
        }
    }
});

var Player = cc.Sprite.extend({
    init:function() {
        this._super("res/hero.png");
        this.setScale(0.5);
        this.scheduleUpdate();
//        this.setAnchorPoint(this._rect.width / 2, this._rect.height / 2);
    },

    flap:function() {
        this.runAction(cc.EaseElasticOut.create(cc.MoveBy.create(1, new cc.Point(0, flapY))));
        this.runAction(cc.Sequence.create(
            cc.EaseElasticOut.create(cc.RotateTo.create(0.5, flapRot)),
            cc.RotateTo.create(0.5, downRot)
        ));
    },

    dropDown:function() {
        var trash = trashFactory.createNextTrash(this);
        if(!trash) return;
        var size = screenSize();
        trash.runAction(cc.EaseSineIn.create(cc.MoveBy.create(1, new cc.Point(0, -trash.getPosition().y), 0.1)));
        return trash;
    },
    update:function() {
        if(this._position.y <= bucketLevel) {
            Game.onPlayerWithBarrier(this, null);
            //game over
            return;
        }
        var clouds = Background.clouds;
        var barriers = Background.barriers;
        var thisRect = collideRect(this);
        for (var i = 0; i < clouds.length; i++) {
            var cloud = clouds[i];
            if(cc.rectIntersectsRect(thisRect, collideRect(cloud))) {
                Game.onPlayerWithCloud(this, cloud);
            }
        }
        for (var i = 0; i < barriers.length; i++) {
            var barrier = barriers[i];
            if(cc.rectIntersectsRect(thisRect, collideRect(barrier))) {
                Game.onPlayerWithBarrier(this, barrier);
            }
        }

    }
});


/**
 * Create player in position
 * @param position {cc.Point}
 */
Player.create = function(position) {
    var player = new Player();
    player.init();
    player.setPosition(position);
    return player;
};

var HeroLayer = cc.Layer.extend({
    isKeyDown:false,
    helloImg:null,
    helloLabel:null,
    circle:null,
    hero:null,
    lazyLayer:null,

    init:function () {
        //////////////////////////////
        // 1. super init first
        this._super();
        lazyLayer = cc.Layer.create();
        this.addChild(lazyLayer);

        this.setTouchEnabled(true);
        this.setKeyboardEnabled(true);
        this.scheduleUpdate();
        return true;
    },
    start:function() {
        var size = screenSize();
        if(this.hero) {
            removeSelf(this.hero);
        }
        this.hero = Player.create(new cc.Point(startX, size.height / 2));

        this.hero.runAction(cc.RepeatForever.create(cc.MoveBy.create(1, new cc.Point(0, downSpeed))));
        this.hero.runAction(cc.RotateTo.create(0.5, downRot));
        lazyLayer.addChild(this.hero, 0);
    },
    // a selector callback
    menuCloseCallback:function (sender) {
        cc.Director.getInstance().end();
    },
    onTouchesBegan:function (touches, event) {
        this.isKeyDown = true;
        if(touches.length <= 1)
            this.hero.flap();
        else
            this.hero.dropDown();
    },
    onTouchesMoved:function (touches, event) {
        if (this.isKeyDown) {
            if (touches) {

                //this.circle.setPosition(touches[0].getLocation().x, touches[0].getLocation().y);
            }
        }
    },
    onTouchesEnded:function (touches, event) {
        this.isKeyDown = false;
    },
    onTouchesCancelled:function (touches, event) {
        console.log("onTouchesCancelled");
    },

    onKeyDown:function(keyCode) {
        if(this.isKeyDown) return;
        if (keyCode == cc.KEY.space) {
            var trash = this.hero.dropDown();
            this.addChild(trash);
        } else if (keyCode = cc.KEY.up) {
            this.hero.flap();
        }
        this.isKeyDown = true;
    },

    onKeyUp:function(keyCode) {
        this.isKeyDown = false;
    },

    update:function (dt) {
//        cc.log("update");
    }

});