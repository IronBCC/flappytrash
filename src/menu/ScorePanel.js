/**
 * Created by IronBCC on 08.06.2014.
 */
const EarthType = {
    GOOD:{
        path:"res/earth_1.png"
    },
    NORMAL:{
        path:"res/earth_3.png"
    },
    BAD:{
        path:"res/earth_6.png"
    }
}


var Earth = cc.Sprite.extend({
    type:null,
//    sprite:null,
    setType:function(type) {
        if(this.sprite) {
            removeSelf(this.sprite);
        }
        this.type = type;
//        this.sprite = cc.Sprite.create(type.path);
        this.init(type.path);
        this.setScale(0.4);
        this.runAction(cc.RepeatForever.create(cc.RotateBy.create(1, 15)));
    }
});
var ScorePanel = cc.Layer.extend({
    earth:null,
    onEnter:function() {
        this._super();
        this.earth = new Earth();
        this.earth.setType(EarthType.BAD);
        var size = screenSize();
        this.earth.setPosition(size.width - 40, size.height - 40);
        this.addChild(this.earth);
    },
    updateScore:function(currentScore) {
        if(currentScore < 100) {
            this.earth.setType(EarthType.BAD);
        } else if(currentScore < 200) {
            this.earth.setType(EarthType.NORMAL);
        } else {
            this.earth.setType(EarthType.GOOD);
        }
    }
});

ScorePanel.create = function() {
    var scorePanel = new ScorePanel();
    scorePanel.init();
    return scorePanel;
}